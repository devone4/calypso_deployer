/**
*  Calypso Deployer CLI. Copyright (C) 2024  Dev One Consulting S.L.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package es.devone.calypso.deployer;

/**
 * Dev One's Calypso Deployer Exception.
 * 
 * @author luis.aguilar@devone.es
 * @version 1.0.1
 */
public class CalypsoDeployerException extends Exception {

	private static final long serialVersionUID = 8528600814375239719L;

	public CalypsoDeployerException(final String message) {
		super(message);
	}

	public CalypsoDeployerException(final Exception e) {
		super(e);
	}
}
