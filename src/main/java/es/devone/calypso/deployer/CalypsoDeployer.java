/**
*  Calypso Deployer CLI. Copyright (C) 2024  Dev One Consulting S.L.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package es.devone.calypso.deployer;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.ant.compress.taskdefs.Unzip;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dev One's Calypso Deployer.
 * 
 * @author luis.aguilar@devone.es
 * @version 1.0.3
 */
public class CalypsoDeployer {

	private static Logger logger = LoggerFactory.getLogger(CalypsoDeployer.class);

	private static final String TIMESTAMP_FORMAT = "yyyyMMddHHmmss";
	private static final String DEPLOY_REMOTE = "deployRemote.%s -Penv=%s -PcalypsoHome=%s -Dorg.gradle.java.home=%s --info --no-daemon";
	private static final String DEPLOY_LOCAL = "deployLocal.%s -Penv=%s -PcalypsoHome=%s -Dorg.gradle.java.home=%s --info --no-daemon";
	private static final String CREATE_CUP = "%s/gradle createCup -Penv=%s -PcalypsoHome=%s -Dorg.gradle.java.home=%s --info --no-daemon";
	private static final String PATCH = "patch %s --yes";

	private final CalypsoDeployerProperties deployerProperties;

	public CalypsoDeployer(final CalypsoDeployerProperties deployerProperties) {
		this.deployerProperties = deployerProperties;
	}

	public void execute() throws CalypsoDeployerException {

		prepareTempDirectory();
		installCalypsoTemplates();
		installDeployLocalConfigPropertiesFile();
		if (!deployerProperties.isBiab()) {
			deleteBuildDirectories();
			createCup();
			patchCup();
		}
		deployLocal();
		deployRemote();
		prepareReleaseDirectory();
		installCalypso();
		replaceCalypsoHome();
	}

	private File asFile(final String fileName) {
		return new File(fileName);
	}

	private File asFile(final File baseDir, final String fileName) {
		return new File(baseDir, fileName);
	}

	private void deleteDirIfExists(final File dir) {
		if (dir.exists()) {
			logger.info("Deleting directory {}", dir.getAbsolutePath());
			if (!FileUtils.deleteQuietly(dir)) {
				logger.error("Could not delete directory {}", dir.getAbsolutePath());
			}
		}
	}

	private void prepareTempDirectory() throws CalypsoDeployerException {

		logger.info("Preparing temp directory...");

		try {
			final File tempDir = asFile(deployerProperties.getTempDir());
			deleteDirIfExists(tempDir);
			tempDir.mkdirs();
			logger.info("Created directory {}", tempDir.getAbsolutePath());

			logger.info("Copying vanilla directory to calypso-stage directory. This could take a while...");

			final File calypsoVanillaDir = asFile(deployerProperties.getCalypsoVanillaDir());
			final File calypsoStageDir = asFile(tempDir, "calypso-stage");
			FileUtils.copyDirectory(calypsoVanillaDir, calypsoStageDir);

			logger.info("Copied {} to {}", calypsoVanillaDir.getAbsolutePath(), calypsoStageDir.getAbsolutePath());

			deployerProperties.setCalypsoStageDir(calypsoStageDir.getAbsolutePath());

		} catch (final IOException e) {
			throw new CalypsoDeployerException(e);
		}
	}

	private void installCalypsoTemplates() throws CalypsoDeployerException {

		if (!deployerProperties.hasCalypsoTemplatesDir()) {
			return;
		}

		logger.info("Installing calypso-templates...");

		try {
			final File calypsoTemplatesSrcDir = asFile(deployerProperties.getCalypsoTemplatesDir());
			final File calypsoStageDir = asFile(deployerProperties.getCalypsoStageDir());
			final File calypsoDestinationTemplatesDstDir = asFile(calypsoStageDir, "tools/calypso-templates");
			FileUtils.copyDirectory(calypsoTemplatesSrcDir, calypsoDestinationTemplatesDstDir);

		} catch (final IOException e) {
			throw new CalypsoDeployerException(e);
		}
	}

	private void installDeployLocalConfigPropertiesFile() throws CalypsoDeployerException {

		if (!deployerProperties.hasCalypsoDeployLocalConfigPropertiesFile()) {
			return;
		}

		logger.info("Installing deployLocalConfig.properties...");

		final File calypsoDeployLocalConfigPropertiesFile = asFile(
				deployerProperties.getCalypsoDeployLocalConfigPropertiesFile());
		final File calypsoStageDir = asFile(deployerProperties.getCalypsoStageDir());
		final File calypsoLocalDeployerConfigDir = asFile(calypsoStageDir, "tools/local-deployer/config");

		try {
			FileUtils.copyFileToDirectory(calypsoDeployLocalConfigPropertiesFile, calypsoLocalDeployerConfigDir);
		} catch (final IOException e) {
			throw new CalypsoDeployerException(e);
		}
	}

	private void deleteBuildDirectories() {

		logger.info("Deleting build directories...");

		final File calypsoCustomExtensionsDir = asFile(deployerProperties.getCalypsoCustomExtensionsDir());

		deleteDirIfExists(asFile(calypsoCustomExtensionsDir, "custom-projects/build"));
		deleteDirIfExists(asFile(calypsoCustomExtensionsDir, "custom-projects/custom-client/build"));
		deleteDirIfExists(asFile(calypsoCustomExtensionsDir, "custom-projects/custom-dataserver-services/build"));
		deleteDirIfExists(asFile(calypsoCustomExtensionsDir, "custom-projects/custom-engine/build"));
		deleteDirIfExists(asFile(calypsoCustomExtensionsDir, "custom-projects/custom-shared-lib/build"));
	}

	private void createCup() throws CalypsoDeployerException {

		logger.info("Creating cup file...");

		final String env = deployerProperties.getCalypsoEnv();
		final String calypsoHome = deployerProperties.getCalypsoStageDir();
		final String javaHome = deployerProperties.getJavaHome();
		final String gradleHome = deployerProperties.getGradleHome();
		final File workingDir = asFile(deployerProperties.getCalypsoCustomExtensionsDir());

		final String line = String.format(CREATE_CUP, gradleHome, env, calypsoHome, javaHome);
		final int exitValue = executeCommand(line, workingDir);
		if (exitValue != 0) {
			throw new CalypsoDeployerException("createCup finished with exit value " + exitValue);
		}

		final File distributionsDir = asFile(workingDir, "build/distributions");

		final File cupFile = distributionsDir.listFiles((dir, name) -> name.toLowerCase().endsWith(".cup"))[0];

		logger.info("Generated cup file {}", cupFile.getAbsolutePath());

		deployerProperties.setCalypsoCupFile(cupFile.getAbsolutePath());
	}

	private void patchCup() throws CalypsoDeployerException {

		logger.info("Patching cup file...");

		final File cupFile = asFile(deployerProperties.getCalypsoCupFile());
		final File workingDir = asFile(deployerProperties.getCalypsoStageDir());
		try {
			FileUtils.copyFileToDirectory(cupFile, workingDir);
		} catch (final IOException e) {
			throw new CalypsoDeployerException(e);
		}

		final String line = String.format(PATCH, cupFile.getName());
		final int exitValue = executeCommand(line, workingDir);
		if (exitValue != 0) {
			throw new CalypsoDeployerException("patchCup finished with exit value " + exitValue);
		}
	}

	private void deployLocal() throws CalypsoDeployerException {

		logger.info("Deploy local...");

		final File workingDir = asFile(deployerProperties.getCalypsoStageDir());

		final String env = deployerProperties.getCalypsoEnv();
		final String calypsoHome = deployerProperties.getCalypsoStageDir();
		final String javaHome = deployerProperties.getJavaHome();
		final String ext = isWindows() ? "bat" : "sh";

		final String line = String.format(DEPLOY_LOCAL, ext, env, calypsoHome, javaHome);
		final int exitValue = executeCommand(line, workingDir);
		if (exitValue != 0) {
			throw new CalypsoDeployerException("deployLocal finished with exit value" + exitValue);
		}
	}

	private void deployRemote() throws CalypsoDeployerException {

		logger.info("Deploy remote...");

		final File workingDir = asFile(deployerProperties.getCalypsoStageDir());

		final String env = deployerProperties.getCalypsoEnv();
		final String calypsoHome = deployerProperties.getCalypsoStageDir();
		final String javaHome = deployerProperties.getJavaHome();
		final String ext = isWindows() ? "bat" : "sh";

		final String line = String.format(DEPLOY_REMOTE, ext, env, calypsoHome, javaHome);
		final int exitValue = executeCommand(line, workingDir);
		if (exitValue != 0) {
			throw new CalypsoDeployerException("deployRemote finished with exit value" + exitValue);
		}

		final File remoteDeployDir = asFile(workingDir, "deploy-remote/" + env);

		deployerProperties.setCalypsoDeployRemoteDir(remoteDeployDir.getAbsolutePath());
	}

	private void prepareReleaseDirectory() throws CalypsoDeployerException {

		logger.info("Preparing release directory...");

		final File releaseDir = asFile(deployerProperties.getCalypsoReleaseDir());
		if (releaseDir.exists()) {
			final SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_FORMAT);
			final String timestamp = sdf.format(new Date());
			final File archiveDir = asFile(releaseDir.getAbsolutePath() + "_" + timestamp);
			logger.info("Archiving old release to {}", archiveDir.getAbsolutePath());
			try {
				FileUtils.moveDirectory(releaseDir, archiveDir);
			} catch (final IOException e) {
				throw new CalypsoDeployerException(e);
			}
		} else {
			releaseDir.mkdirs();
		}
	}

	private void installCalypso() {

		logger.info("Installing Calypso...");

		final File calypsoReleaseDir = asFile(deployerProperties.getCalypsoReleaseDir());
		final File remoteDeployDir = asFile(deployerProperties.getCalypsoDeployRemoteDir());
		final File[] zipFiles = remoteDeployDir.listFiles((dir, name) -> name.toLowerCase().endsWith(".zip"));
		final Unzip unzip = new Unzip();
		for (int i = 0; i < zipFiles.length; i++) {
			final File zipFile = zipFiles[i];
			logger.info("Unzip {} into {}", zipFile.getAbsolutePath(), calypsoReleaseDir.getAbsolutePath());
			unzip.setSrc(zipFile);
			unzip.setDest(calypsoReleaseDir);
			unzip.execute();
		}
	}

	private void replaceCalypsoHome() throws CalypsoDeployerException {

		logger.info("Replacing CALYPSO_HOME in scripts...");

		final File calypsoReleaseDir = asFile(deployerProperties.getCalypsoReleaseDir());
		final File binDir = asFile(calypsoReleaseDir, "bin");
		final File[] scriptFiles = binDir
				.listFiles((dir, name) -> (name.toLowerCase().endsWith(".bat") || name.toLowerCase().endsWith(".sh")));

		for (int i = 0; i < scriptFiles.length; i++) {

			final File scriptFile = scriptFiles[i];
			final String target = deployerProperties.getCalypsoStageDir();
			final String replacement = deployerProperties.getCalypsoReleaseDir();

			logger.info("Replacing {} by {} in file {}", target, replacement, scriptFile.getAbsolutePath());

			try {
				String content = FileUtils.readFileToString(scriptFile, StandardCharsets.UTF_8);
				content = content.replace(target, replacement);
				FileUtils.writeStringToFile(scriptFile, content, StandardCharsets.UTF_8);
			} catch (final IOException e) {
				throw new CalypsoDeployerException(e);
			}
		}
	}

	private int executeCommand(final String line, final File workingDir) throws CalypsoDeployerException {

		final StringBuilder command = new StringBuilder();
		if (isWindows()) {
			command.append("cmd /C");
		} else {
			command.append("sh");
		}
		command.append(" ");
		command.append(line);

		logger.info("Executing: {}", command);

		final CommandLine cmd = CommandLine.parse(command.toString());
		final DefaultExecutor executor = DefaultExecutor.builder().setWorkingDirectory(workingDir).get();
		try {
			return executor.execute(cmd);
		} catch (final IOException e) {
			throw new CalypsoDeployerException(e);
		}
	}

	private boolean isWindows() {
		return System.getProperty("os.name").startsWith("Windows");
	}
}
