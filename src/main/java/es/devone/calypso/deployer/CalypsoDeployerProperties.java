/**
*  Calypso Deployer CLI. Copyright (C) 2024  Dev One Consulting S.L.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package es.devone.calypso.deployer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dev One's Calypso Deployer Properties.
 * 
 * @author luis.aguilar@devone.es
 * @version 1.0.3
 */
public class CalypsoDeployerProperties {

	private static Logger logger = LoggerFactory.getLogger(CalypsoDeployerProperties.class);

	private static final String CALYPSO_CUP_FILE = "calypsoCupFile";
	private static final String CALYPSO_CUSTOM_EXTENSIONS_DIR = "calypsoCustomExtensionsDir";
	private static final String CALYPSO_DEPLOY_LOCAL_CONFIG_PROPERTIES_FILE = "calypsoDeployLocalConfigPropertiesFile";
	private static final String CALYPSO_DEPLOY_REMOTE_DIR = "calypsoDeployRemoteDir";
	private static final String CALYPSO_ENV = "calypsoEnv";
	private static final String CALYPSO_RELEASE_DIR = "calypsoReleaseDir";
	private static final String CALYPSO_STAGE_DIR = "calypsoStageDir";
	private static final String CALYPSO_TEMPLATES_DIR = "calypsoTemplatesDir";
	private static final String CALYPSO_VANILLA_DIR = "calypsoVanillaDir";
	private static final String GRADLE_HOME = "gradleHome";
	private static final String JAVA_HOME = "javaHome";
	private static final String IS_BIAB = "isBiab";
	private static final String TEMP_DIR = "tempDir";

	private static final String EMPTY = "";

	private final Properties properties = new Properties();

	public CalypsoDeployerProperties(final String fileName) throws CalypsoDeployerException {

		try (FileInputStream is = new FileInputStream(fileName)) {
			properties.load(is);
		} catch (final IOException e) {
			throw new CalypsoDeployerException(e);
		}

		if (properties.isEmpty()) {
			throw new CalypsoDeployerException("Properties is empty");
		}

		logger.info("Loaded properties from {} ", fileName);

		logger.info("calypsoEnv = {}", getProperty(CALYPSO_ENV));
		logger.info("tempDir = {}", getProperty(TEMP_DIR));
		logger.info("calypsoCustomExtensionsDir = {}", getProperty(CALYPSO_CUSTOM_EXTENSIONS_DIR));
		logger.info("calypsoDeployLocalConfigPropertiesFile = {}",
				getProperty(CALYPSO_DEPLOY_LOCAL_CONFIG_PROPERTIES_FILE));
		logger.info("calypsoTemplatesDir = {}", getProperty(CALYPSO_TEMPLATES_DIR));
		logger.info("calypsoVanillaDir = {}", getProperty(CALYPSO_VANILLA_DIR));
		logger.info("calypsoReleaseDir = {}", getProperty(CALYPSO_RELEASE_DIR));
		logger.info("javaHome = {}", getProperty(JAVA_HOME));

		final File gradleHome = new File(getCalypsoVanillaDir(), "tools/gradle/bin");
		setGradleHome(gradleHome.getAbsolutePath());
	}

	private String getProperty(final String key) {
		return properties.getProperty(key, EMPTY);
	}

	private String getFileProperty(final String key) {
		return getProperty(key).replace("\\", File.separator);
	}

	public boolean isBiab() {
		return Boolean.valueOf(getProperty(IS_BIAB));
	}

	public String getCalypsoCupFile() {
		return getFileProperty(CALYPSO_CUP_FILE);
	}

	public String getCalypsoCustomExtensionsDir() {
		return getFileProperty(CALYPSO_CUSTOM_EXTENSIONS_DIR);
	}

	public String getCalypsoDeployLocalConfigPropertiesFile() {
		return getFileProperty(CALYPSO_DEPLOY_LOCAL_CONFIG_PROPERTIES_FILE);
	}

	public boolean hasCalypsoDeployLocalConfigPropertiesFile() {
		return !EMPTY.equals(getCalypsoDeployLocalConfigPropertiesFile());
	}

	public String getCalypsoDeployRemoteDir() {
		return getFileProperty(CALYPSO_DEPLOY_REMOTE_DIR);
	}

	public String getCalypsoEnv() {
		return getProperty(CALYPSO_ENV);
	}

	public String getCalypsoReleaseDir() {
		return getFileProperty(CALYPSO_RELEASE_DIR);
	}

	public String getCalypsoStageDir() {
		return getFileProperty(CALYPSO_STAGE_DIR);
	}

	public String getCalypsoTemplatesDir() {
		return getFileProperty(CALYPSO_TEMPLATES_DIR);
	}

	public boolean hasCalypsoTemplatesDir() {
		return !EMPTY.equals(getCalypsoTemplatesDir());
	}

	public String getCalypsoVanillaDir() {
		return getFileProperty(CALYPSO_VANILLA_DIR);
	}

	public String getGradleHome() {
		return getFileProperty(GRADLE_HOME);
	}

	public String getJavaHome() {
		return getFileProperty(JAVA_HOME);
	}

	public String getTempDir() {
		return getFileProperty(TEMP_DIR);
	}

	public void setCalypsoCupFile(final String value) {
		properties.setProperty(CALYPSO_CUP_FILE, value);
	}

	public void setCalypsoDeployRemoteDir(final String value) {
		properties.setProperty(CALYPSO_DEPLOY_REMOTE_DIR, value);
	}

	public void setCalypsoStageDir(final String value) {
		properties.setProperty(CALYPSO_STAGE_DIR, value);
	}

	public void setGradleHome(final String value) {
		properties.setProperty(GRADLE_HOME, value);
	}

}
