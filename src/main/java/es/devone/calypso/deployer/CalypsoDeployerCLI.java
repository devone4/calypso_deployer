/**
*  Calypso Deployer CLI. Copyright (C) 2024  Dev One Consulting S.L.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package es.devone.calypso.deployer;

import java.time.Duration;
import java.time.Instant;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dev One's Calypso Deployer CLI (Command Line Interface).
 * 
 * @author luis.aguilar@devone.es
 * @version 1.0.3
 */
public class CalypsoDeployerCLI {

	private static Logger logger = LoggerFactory.getLogger(CalypsoDeployerCLI.class);

	private static final String CONFIG_PROPERTIES = "configProperties";
	private static final String VERSION = "1.0.3";

	public static void main(final String[] args) throws CalypsoDeployerException {

		logger.info("Dev One's Calypso Deployer v" + VERSION);

		final Instant startTime = Instant.now();

		logger.info("Loading config.properties...");

		final Options options = new Options();
		options.addOption(new Option("c", CONFIG_PROPERTIES, true, "The path to the config.properties file"));

		final CommandLineParser parser = new DefaultParser();
		CommandLine cmd = null;
		try {
			cmd = parser.parse(options, args);
		} catch (final ParseException e) {
			throw new CalypsoDeployerException(e);
		}

		if (!cmd.hasOption(CONFIG_PROPERTIES)) {
			throw new CalypsoDeployerException("Missing argument --" + CONFIG_PROPERTIES);
		}

		try {
			final CalypsoDeployerProperties deployerProperties = new CalypsoDeployerProperties(
					cmd.getOptionValue(CONFIG_PROPERTIES));
			final CalypsoDeployer app = new CalypsoDeployer(deployerProperties);
			app.execute();
		} catch (final CalypsoDeployerException e) {
			logger.error(e.getMessage());
			System.exit(1);
		}

		final Duration duration = Duration.between(startTime, Instant.now());

		logger.info("DEPLOY SUCCESSFUL in {}", duration);

		System.exit(0);
	}
}
