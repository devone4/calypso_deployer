# Dev One's Calypso Deployer

## About

Utility to deploy Calypso in a full automated way:

- [x] Deletes the build directories at custom-extensions
- [x] Compiles the custom code and creates the cup file
- [x] Patches the cup file
- [x] Executes deployLocal
- [x] Executes deployRemote
- [x] Archives the current release directory
- [x] Installs Calypso in the release directory

The result is a ready-to-use Calypso installation:

![release](release.png)

## Prerequisites

* [Java 1.8+](https://www.oracle.com/es/java/technologies/javase/javase8-archive-downloads.html)

## Build & Run

1. Build:

```bash
	build.bat
```

2. Configure the [config.properties](config/config.properties):

3. Run:

```bash
	java -jar devone-calypso-deployer-<version>.jar --configProperties config.properties
```

## Patches and pull requests

Your patches are welcome. Here's our suggested workflow:
 
* Fork the project.
* Make your feature addition or bug fix.
* Send us a pull request with a description of your work.

## Built With

* [Gradle](https://gradle.org/) - Build

## Questions

If you have any questions, please contact us [here](mailto:contact@devone.es)

## License

This project is licensed under the GNU General Public License v3 - see the [LICENSE.md](LICENSE.md) file for details
